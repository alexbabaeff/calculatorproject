package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
// Variables

    String currentNumberString = "0";
    double previousNumber;
    String pendingOperation = null;
    boolean clearDisplayOnNextDigit = false;
    boolean currentNumberStringHasDecimal = false;
    String lastNumber;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView displayTextView = findViewById(R.id.textViewDisplay);
        //insert our code
        //connect the xml

        //done
        Button allClearButton = findViewById(R.id.AllClear );
        allClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //this code will executed when button is tapped
                System.out.println("All Clear was tapped");

                //clear or reset all variables
                currentNumberString = "0";
                previousNumber = 0.0;
                pendingOperation = null;
                clearDisplayOnNextDigit = false;
                currentNumberStringHasDecimal = false;

                //Display the current number
                displayTextView.setText(currentNumberString);

            }
        });


        Button changeSignButton = findViewById(R.id.SignChange);
        changeSignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Sign Change was clicked");

                //is the current number a negative
                if(currentNumberString.startsWith("-")){
                    //negative so need to remove the negative sign
                    currentNumberString = currentNumberString.replace("-", "");

                }
                else {
                    //positive need to add "-"
                    currentNumberString = "-" + currentNumberString;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);

            }
        });


        Button divideOperationButton = findViewById(R.id.divideOperation);
        divideOperationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Divide operation was pressed");
                if (pendingOperation != null){

                    double currentNumber = Double.parseDouble(currentNumberString);

                    //perform the pending operation on the previous number and current number
                    if(pendingOperation.equals("Add")){
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract")){
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply")){
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide")){
                        previousNumber /= currentNumber;
                    }

                    // Replace current number displayed with the result
                    currentNumberString = Double.valueOf(previousNumber).toString();

                    //Display new current number
                    displayTextView.setText(currentNumberString);
                }

                //save current number as the previous number
                previousNumber = Double.parseDouble(currentNumberString);

                //remember the selected operator as the pending operator
                pendingOperation = "Divide";

                //clear the display on the next digit input
                clearDisplayOnNextDigit = true;
            }
        });

        //done
        Button seven = findViewById(R.id.seven);
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Seven was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "7";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){


                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }


                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button eight = findViewById(R.id.eight);
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("eight was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "8";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button nine = findViewById(R.id.nine);
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("nine was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "9";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        Button multiplyOperation = findViewById(R.id.multiplyOperation);
        multiplyOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("multiply was pressed");
                if (pendingOperation != null){

                    double currentNumber = Double.parseDouble(currentNumberString);

                    //perform the pending operation on the previous number and current number
                    if(pendingOperation.equals("Add")){
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract")){
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply")){
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide")){
                        previousNumber /= currentNumber;
                    }

                    // Replace current number displayed with the result
                    currentNumberString = Double.valueOf(previousNumber).toString();

                    //Display new current number
                    displayTextView.setText(currentNumberString);
                }

                //save current number as the previous number
                previousNumber = Double.parseDouble(currentNumberString);

                //remember the selected operator as the pending operator
                pendingOperation = "Multiply";

                //clear the display on the next digit input
                clearDisplayOnNextDigit = true;
            }
        });

        //done
        Button four = findViewById(R.id.four);
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Four was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "4";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button five = findViewById(R.id.five);
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("five was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "5";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button six = findViewById(R.id.six);
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("six was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "6";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        Button subtractOperation = findViewById(R.id.subtractOperation);
        subtractOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Subtract was pressed");
                if (pendingOperation != null){

                    double currentNumber = Double.parseDouble(currentNumberString);

                    //perform the pending operation on the previous number and current number
                    if(pendingOperation.equals("Add")){
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract")){
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply")){
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide")){
                        previousNumber /= currentNumber;
                    }

                    // Replace current number displayed with the result
                    currentNumberString = Double.valueOf(previousNumber).toString();

                    //Display new current number
                    displayTextView.setText(currentNumberString);
                }

                //save current number as the previous number
                previousNumber = Double.parseDouble(currentNumberString);

                //remember the selected operator as the pending operator
                pendingOperation = "Subtract";

                //clear the display on the next digit input
                clearDisplayOnNextDigit = true;
            }
        });

         //done
        Button one = findViewById(R.id.one);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("one was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }
                //append digit to the right of current number
                final String enteredDigit = "1";
                lastNumber = "1";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button two = findViewById(R.id.two);
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("two was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "2";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append number to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        //done
        Button three = findViewById(R.id.three);
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("three was pressed");

                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "3";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);
            }
        });

        Button addOperation = findViewById(R.id.addOperation);
        addOperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("add was pressed");

                if (pendingOperation != null){

                    double currentNumber = Double.parseDouble(currentNumberString);

                    //perform the pending operation on the previous number and current number
                    if(pendingOperation.equals("Add")){
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract")){
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply")){
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide")){
                        previousNumber /= currentNumber;
                    }

                    // Replace current number displayed with the result
                    currentNumberString = Double.valueOf(previousNumber).toString();

                    //Display new current number
                    displayTextView.setText(currentNumberString);
                }

                //save current number as the previous number
                previousNumber = Double.parseDouble(currentNumberString);

                //remember the selected operator as the pending operator
                pendingOperation = "Add";

                //clear the display on the next digit input
                clearDisplayOnNextDigit = true;

            }
        });

        //done
        Button zero = findViewById(R.id.zero);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("zero was pressed");


                //check if we need to clear the display on the next digit input
                if (clearDisplayOnNextDigit){
                    currentNumberString = "0";
                    clearDisplayOnNextDigit = false;
                    currentNumberStringHasDecimal = false;
                }

                //append digit to the right of current number
                final String enteredDigit = "0";
                if (currentNumberString.equals("0")){
                    //if display is zero replace with digit
                    currentNumberString = enteredDigit;
                }
                else if(currentNumberString.length()>9){

                }
                else{
                    // If display is not zero then append "1 to whatever number is displayed
                    currentNumberString += enteredDigit;
                }

                //Display the current number
                displayTextView.setText(currentNumberString);

            }
        });

        Button decimalButton = findViewById(R.id.decimal);
        decimalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("decimal was pressed");

                // does the current number have a decimal?
                if (!currentNumberStringHasDecimal){

                    //check if we need to clear the display on the next digit input
                    if (clearDisplayOnNextDigit){
                        currentNumberString = "0";
                        clearDisplayOnNextDigit = false;
                    }

                    //append digit to the right of current number
                    final String enteredDigit = ".";
                    if (currentNumberString.equals("0")){
                        //if display is zero replace with digit
                        currentNumberString = enteredDigit;
                    }
                    else{
                        // If display is not zero then append "1 to whatever number is displayed
                        currentNumberString += enteredDigit;
                    }

                    //Display the current number
                    displayTextView.setText(currentNumberString);
                    //set flag to indicate number has decimal
                    currentNumberStringHasDecimal = true;
                }
            }
        });

        Button equals = findViewById(R.id.equals);
        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("equals was pressed");

                if (pendingOperation != null){

                    double currentNumber = Double.parseDouble(currentNumberString);

                    //perform the pending operation on the previous number and current number
                    if(pendingOperation.equals("Add")){
                        previousNumber += currentNumber;
                    }
                    else if (pendingOperation.equals("Subtract")){
                        previousNumber -= currentNumber;
                    }
                    else if (pendingOperation.equals("Multiply")){
                        previousNumber *= currentNumber;
                    }
                    else if (pendingOperation.equals("Divide")){
                        previousNumber /= currentNumber;
                    }

                    // Replace current number displayed with the result
                    currentNumberString = Double.valueOf(previousNumber).toString();

                    //Display new current number
                    displayTextView.setText(currentNumberString);
                }

                //clear pending operation
                pendingOperation = null;

                //clear the display on the next digit input
                clearDisplayOnNextDigit = true;

            }
        });

        Button delete = findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("delete was pressed");

                if (currentNumberString.length() == 1 ){

                    currentNumberString = displayTextView.getText().toString();
                    currentNumberString = currentNumberString.substring(0, currentNumberString.length() - 1);
                    displayTextView.setText(currentNumberString + "0");
                }
                else if(currentNumberString.equals("0")) {


                }
                else{
                    currentNumberString = displayTextView.getText().toString();
                    currentNumberString = currentNumberString.substring(0, currentNumberString.length() - 1);
                    displayTextView.setText(currentNumberString);
                }






            }
        });

        Button percent = findViewById(R.id.percent);
        percent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("percent was pressed");

                double currentNumber = Double.parseDouble(currentNumberString);

                currentNumber /= 100;

                currentNumberString = Double.valueOf(currentNumber).toString();

                displayTextView.setText(currentNumberString);




            }
        });


    }
}